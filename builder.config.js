const { VueLoaderPlugin } = require("vue-loader");

module.exports = {
	webpack: function (config) {
		config.resolve.extensions = [
			...config.resolve.extensions,
			'.vue'
		];
		config.module.rules = [
			...config.module.rules,
			{
				test: /\.vue$/,
				loader: 'vue-loader',
				options: {
					hotReload: false
				}
			},
			{
				test: /\.svg$/,
				use: [
					'babel-loader',
					'vue-svg-loader'
				]
			}
		];
		config.plugins = [
			...config.plugins,
			new VueLoaderPlugin()
		];
		return config;
	},
	postcss: function (plugins) {
		return [
			...plugins
		];
	}
};
