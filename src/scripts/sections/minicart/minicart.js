import { register } from "@shopify/theme-sections";
import Vue from "vue";
import { store } from "../../vue/store";
import { ACTION_INIT_CART } from "../../vue/store/cart/cart";
import Minicart from "../../vue/Minicart";

document.addEventListener("product:add", async function (event) {
	const detail = event.detail;

	if (!detail) {
		return null;
	}

	const items = detail.items;

	if (!items) {
		return null;
	}

	const errorCallback =
		typeof event.detail.errorCallback === "function"
			? event.detail.errorCallback
			: () => {};

	await store.dispatch(`cart/${ACTION_ADD_TO_CART}`, {
		products: event.detail.items,
		errorCallback
	});

	const callback = detail.callback;

	if (typeof callback === "function") {
		await callback();
	}

	document.dispatchEvent(new CustomEvent("minicart:open"));
});

register("minicart", {
	onLoad: function () {
		this.init();
	},
	init: async function () {
		await store.dispatch(`cart/${ACTION_INIT_CART}`);

		new Vue({
			store,
			render: (h) => h(Minicart)
		}).$mount(this.container);
	}
});
