import "unfetch/polyfill";
import "es6-promise/auto";

import objects from "./core/objects";
import translations from "@savchukoleksii/shopify-theme-translations-tool";
import settings from "@savchukoleksii/shopify-theme-settings-tool";
import * as sections from "@shopify/theme-sections";

import * as bodyScrollLock from "body-scroll-lock";
global.bodyScrollLock = bodyScrollLock;

import $ from "jquery";
global.jQuery = $;
global.$ = $;

import "./vue/filters";
import { store } from "./vue/store";
import { ACTION_INIT_CART } from "./vue/store/cart/cart";
import { ACTION_INIT_WISHLIST } from "./vue/store/wishlist/wishlist";

window.theme = window.theme || {};

const DOMContentLoadedPromise = new Promise((resolve) => {
	document.addEventListener("DOMContentLoaded", async () => {
		resolve();
	});
});

/*================ Components ================*/
// require("./components/product-card");

/*================ Sections ================*/
require("./sections/minicart/minicart");

/*================ Templates ================*/
// require("./templated/customer-addresses");

(async () => {
	try {
		await Promise.all([
			translations.load(),
			settings.load(),
			objects.load(),
			DOMContentLoadedPromise
		]);

		document.dispatchEvent(new CustomEvent("theme:all:loaded"));
	} catch (error) {}

	await Promise.all([
		store.dispatch(`cart/${ACTION_INIT_CART}`),
		store.dispatch(`wishlist/${ACTION_INIT_WISHLIST}`)
	]);

	sections.load("*");
})();
