import axios from "axios";

export const EVENT_UNKNOWN = 0;
export const EVENT_PRODUCT = 1;
export const EVENT_CATEGORY = 2;
export const EVENT_CART = 3;
export const EVENT_WISHLIST = 4;
export const EVENT_COUPON = 5;

function getVariantImageSrc(product, variant) {
	return (() => {
		if (variant.image) {
			return variant.image.src;
		}

		if (product.featured_image) {
			return product.featured_image.src;
		}

		return null;
	})();
}

export function onWishlistLoad(action = async () => {}) {
	if (!window.SwymCallbacks) {
		window.SwymCallbacks = [];
	}

	window.SwymCallbacks.push(async () => {
		return await action();
	});
}

export async function getProductsByIds(ids = []) {
	const search = encodeURI(
		ids
			.map((id) => {
				return `id:${id}`;
			})
			.join(" OR ")
	);

	const response = (
		await axios.get(`/search?q=${search}&type=product&view=products-json`)
	).data;
	const html = document.createElement("div");
	html.innerHTML = response;
	return JSON.parse(html.querySelector("[data-json-response]").innerHTML);
}

export async function getWishlistProducts() {
	return await new Promise((resolve, reject) => {
		try {
			window._swat.fetch(async (response) => {
				const ids = response.map((item) => {
					return item.empi;
				});

				const wishlistedVariantsIds = response.reduce((items, item) => {
					return {
						...items,
						[item.empi]: [...(items[item.empi] || []), item.epi]
					};
				}, {});

				try {
					const wishlistProducts = (
						await getProductsByIds(ids)
					).reduce((products, product) => {
						return {
							...products,
							[product.id]: {
								...product,
								variants: product.variants.map((variant) => {
									return {
										...variant,
										wishlisted: (
											wishlistedVariantsIds[product.id] ??
											[]
										).includes(variant.id)
									};
								})
							}
						};
					}, {});

					const wishlistedItems = response.reduce((items, item) => {
						return [
							...items,
							{
								...wishlistProducts[item.empi],
								wishlisted_variant_id: item.epi
							}
						];
					}, []);

					return resolve(wishlistedItems);
				} catch (error) {
					return reject(error);
				}
			});
		} catch (error) {
			return reject(error);
		}
	});
}

export async function addToWishlist(product, variant) {
	return await new Promise((resolve, reject) => {
		try {
			if (!variant) {
				variant = product.variants[0];
			}

			window._swat.addToWishList(
				{
					et: EVENT_WISHLIST,
					empi: product.id,
					epi: variant.id || product.id,
					dt: product.title,
					du: `https://${window.Shopify.shop}${product.url}`,
					ct: product.type,
					bt: product.vendor,
					pr: variant.price / 100 || product.price / 100,
					iu: getVariantImageSrc(product, variant)
						.replace("https", "")
						.replace("http", ""),
					variants: [
						{
							[variant.title]: variant.id
						}
					],
					stk: variant.inventory_quantity,
					uri: `https://${window.Shopify.shop}${product.url}`,
					ri: {},
					rc: "default",
					type: "product-variant",
					_cv: true,
					vi: variant.title,
					cprops: {}
				},
				(response) => {
					return resolve(response);
				}
			);
		} catch (error) {
			return reject(error);
		}
	});
}

export async function removeFromWishList(product, variant) {
	return await new Promise((resolve, reject) => {
		try {
			if (!variant) {
				variant = product.variants[0];
			}

			window._swat.removeFromWishList(
				{
					et: -1,
					empi: product.id,
					epi: variant.id || product.id,
					dt: product.title,
					du: `https://${window.Shopify.shop}${product.url}`,
					ct: product.type,
					pr: variant.price / 100 || product.price / 100,
					iu: getVariantImageSrc(product, variant)
						.replace("https", "")
						.replace("http", ""),
					variants: [
						{
							[variant.title]: variant.id
						}
					],
					stk: variant.inventory_quantity,
					uri: `https://${window.Shopify.shop}${product.url}`,
					ri: {},
					rc: "default",
					type: "product-variant",
					_cv: true,
					vi: variant.title
				},
				(response) => {
					return resolve(response);
				}
			);
		} catch (error) {
			return reject(error);
		}
	});
}

export async function getWishlistCount() {
	return await new Promise((resolve, reject) => {
		try {
			window._swat.wishlistCount((response) => {
				return resolve(response);
			});
		} catch (error) {
			return reject(error);
		}
	});
}

export function addProductToWishlistObject(product, current_variant = null) {
	["SwymViewProducts", "SwymWatchProducts", "SwymProductVariants"].forEach(
		function (key) {
			if (!window[key]) {
				window[key] = {};
			}
		}
	);

	let products = {};
	if (!current_variant) {
		current_variant = product.variants[0];
	}

	product.variants.forEach((variant) => {
		let variantData = {
			empi: product.id,
			epi: variant.id,
			du: `https://${window.Shopify.shop}${product.url}`,
			dt: product.title,
			ct: product.type,
			iu: getVariantImageSrc(product, variant)
				.replace("https", "")
				.replace("http", ""),
			stk: variant.inventory_quantity,
			pr: variant.price / 100,
			variants: [
				{
					[variant.title]: variant.id
				}
			]
		};

		if (variant.compare_at_price) {
			variantData = {
				...variantData,
				op: variant.compare_at_price / 100
			};
		}

		window.SwymProductVariants[variant.id] = variantData;

		window.SwymWatchProducts[variant.id] = products[variant.id] = {
			id: variant.id,
			available: variant.available,
			inventory_management: variant.inventory_management,
			inventory_quantity: variant.inventory_quantity,
			title: variant.title,
			inventory_policy: variant.inventory_policy
		};
	});

	let productData = {
		empi: product.id,
		epi: current_variant.id,
		dt: product.title,
		du: `https://${window.Shopify.shop}${product.url}`,
		ct: product.type,
		pr: current_variant.price / 100,
		stk: current_variant.inventory_quantity,
		iu: getVariantImageSrc(product, current_variant)
			.replace("https", "")
			.replace("http", ""),
		variants: [
			{
				[current_variant.title]: current_variant.id
			}
		]
	};

	if (current_variant.compare_at_price) {
		productData = {
			...productData,
			op: current_variant.compare_at_price / 100
		};
	}

	window.SwymViewProducts[product.handle] = window.SwymViewProducts[
		product.id
		] = productData;
	window.SwymWatchProducts[product.handle] = window.SwymWatchProducts[
		product.id
		] = products;
}

export default {
	addProductToWishlistObject,
	onWishlistLoad,
	getWishlistProducts,
	addToWishlist,
	removeFromWishList,
	getWishlistCount
};
