import Cookies from "js-cookie";
import { templateEquals } from "@savchukoleksii/shopify-template-checker";
import axios from "axios";

export function getRecentlyViewedProductsIds() {
	const recently_viewed = Cookies.get("recently_viewed");
	if (!recently_viewed) {
		Cookies.set("recently_viewed", "");
	}

	let ids = recently_viewed
		.split(",")
		.filter((id) => {
			return id;
		})
		.map((id) => +id);

	if (!templateEquals("product")) {
		return ids;
	}

	return ids.filter(function (id) {
		return id !== +window?.meta?.product?.id;
	});
}

export function addToRecentlyViewedProducts(current_id) {
	if (!current_id) {
		return;
	}

	const recently_viewed = Cookies.get("recently_viewed");
	if (!recently_viewed) {
		Cookies.set("recently_viewed", "");
	}

	let recently_viewed_ids = [
		current_id,
		...getRecentlyViewedProductsIds().filter((id) => {
			return id !== current_id;
		})
	];

	Cookies.set("recently_viewed", recently_viewed_ids.join(","));
}

export function removeFromRecentlyViewedProducts(current_id) {
	const recently_viewed = Cookies.get("recently_viewed");
	if (!recently_viewed) {
		Cookies.set("recently_viewed", "");
	}

	let recently_viewed_ids = getRecentlyViewedProductsIds().filter((id) => {
		return +id !== current_id;
	});

	Cookies.set("recently_viewed", recently_viewed_ids.join(","));
}

export function addCurrentProductToRecentlyViewedProducts() {
	if (!templateEquals("product")) {
		return;
	}

	const current_id = +window?.meta?.product?.id;

	addToRecentlyViewedProducts(current_id);
}

export async function getRecentlyViewedProducts() {
	const ids = getRecentlyViewedProductsIds();

	const search = encodeURI(ids.map((id) => `id:${id}`).join(" OR "));

	const response = (
		await axios.get(`/search?q=${search}&type=product&view=products-json`)
	).data;
	const html = document.createElement("div");
	html.innerHTML = response;

	const productsJSON = JSON.parse(
		html.querySelector("[data-json-response]").innerHTML
	);

	let order = ids.reduce((ids, id, index) => {
		return {
			...ids,
			[id]: index
		};
	}, {});

	productsJSON.sort((next, current) => {
		return order[next.id] - order[current.id];
	});

	return productsJSON;
}

export default {
	addToRecentlyViewedProducts,
	removeFromRecentlyViewedProducts,
	addCurrentProductToRecentlyViewedProducts,
	getRecentlyViewedProductsIds,
	getRecentlyViewedProducts
};
