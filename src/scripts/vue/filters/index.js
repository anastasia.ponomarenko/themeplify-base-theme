import Vue from "vue";
import translations from "@savchukoleksii/shopify-theme-translations-tool";
import settings from "@savchukoleksii/shopify-theme-settings-tool";
import objects from "../../core/objects";
import { formatMoney } from "@shopify/theme-currency";
import { getSizedImageUrl } from "@shopify/theme-images";

Vue.filter("money", function (value, format = "${{amount}}") {
	return formatMoney(value, format);
});

Vue.filter("moneyNoDecimals", function (value, format = "${{amount_no_decimals}}") {
	return formatMoney(value, format);
});

Vue.filter("img_url", function (src, size) {
	const settingsAll = settings.all();

	if (!src) {
		if (settingsAll.placeholder) {
			src = settingsAll.placeholder;
		} else {
			return null;
		}
	}

	return getSizedImageUrl(src, size);
});

Vue.filter("asset_img_url", function (file, size = null) {
	const src = objects.get("assetsUrl").replace("%%file%%", file);

	if (!src) {
		return src;
	}

	if (!size) {
		return src;
	}

	return getSizedImageUrl(src, size);
});

Vue.filter("file_img_url", function (file, size = null) {
	const src = objects.get("fileUrl").replace("%%file%%", file);

	if (!src) {
		return src;
	}

	if (!size) {
		return src;
	}

	return getSizedImageUrl(src, size);
});

Vue.filter("t", function (value, params = {}) {
	return translations.get(value, params);
});

Vue.filter("unescape", function unescape(value) {
	const doc = new DOMParser().parseFromString(value, "text/html");

	return doc.documentElement.textContent;
});

Vue.filter("routes", function routes(value) {
	return objects.routes(value);
});

Vue.filter("default_value", function routes(value, defaultValue) {
	if (value) {
		return value;
	}

	return defaultValue;
});

Vue.filter("link_to_vendor", (brand, className = "") => {
	return objects.get("linkToVendor").replaceAll("%%brand%%", brand).replaceAll("%%className%%", className);
});
