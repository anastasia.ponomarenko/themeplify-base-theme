import Vue from "vue";
import Vuex from "vuex";
import cart from "./cart/cart";
import wishlist from "./wishlist/wishlist";

Vue.use(Vuex);

export const store = new Vuex.Store({
	modules: {
		cart,
		wishlist
	}
});
