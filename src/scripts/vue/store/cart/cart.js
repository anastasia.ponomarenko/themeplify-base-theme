import axios from "axios";
import qs from "qs";
import settings from "../../../core/settings";
import { ACTION_ADD_TO_WISHLIST } from "../wishlist/wishlist";

export const STATE_LOADING = "loading";
export const STATE_CART = "cart";
export const STATE_SETTINGS = "settings";

export const GETTER_CART = "cart";
export const GETTER_SETTINGS = "settings";

export const MUTATION_UPDATE_LOADING = "updateLoading";
export const MUTATION_UPDATE_CART = "updateCart";
export const MUTATION_UPDATE_SETTINGS = "updateSettings";

export const ACTION_INIT_CART = "initCart";
export const ACTION_GET_CART = "getCart";
export const ACTION_ADD_TO_CART = "addToCart";
export const ACTION_REMOVE_ITEM = "removeItem";
export const ACTION_CHANGE_QUANTITY = "changeQuantity";
export const ACTION_SAVE_FOR_LATER = "saveForLater";

const api = {
	async getCart() {
		const response = (await axios.get(`/cart?view=object-json`)).data;

		const html = document.createElement("div");
		html.innerHTML = response;

		return JSON.parse(html.querySelector("[data-json-response]").innerHTML);
	},
	addItem(item) {
		return this.addItems([item]).then((response) => response.data);
	},
	addItems(items = []) {
		return axios.post(
			"/cart/add.js",
			qs.stringify({
				items
			}),
			{
				headers: {
					Accept: "*/*",
					"Content-Type":
						"application/x-www-form-urlencoded; charset=UTF-8",
					"X-Requested-With": "XMLHttpRequest"
				}
			}
		);
	},
	changeItem(newItem) {
		return axios
			.post("/cart/change.js", qs.stringify(newItem), {
				headers: {
					Accept: "*/*",
					"Content-Type":
						"application/x-www-form-urlencoded; charset=UTF-8",
					"X-Requested-With": "XMLHttpRequest"
				}
			})
			.then((res) => res.data);
	},
	changeQuantity(key, quantity) {
		return this.changeItem({
			id: key,
			quantity
		});
	},
	removeItem(key) {
		return this.changeQuantity(key, 0);
	}
};

export default {
	namespaced: true,
	state: () => {
		return {
			[STATE_LOADING]: true,
			[STATE_CART]: {
				items: []
			},
			[STATE_SETTINGS]: {}
		};
	},
	getters: {
		[GETTER_CART](state) {
			return state[STATE_CART];
		},
		[GETTER_SETTINGS](state) {
			return state[STATE_SETTINGS];
		}
	},
	mutations: {
		[MUTATION_UPDATE_CART](state, cart) {
			cart.item_count = Object.values(cart.items).reduce(
				(total, item) => {
					return total + item.quantity;
				},
				0
			);

			cart.items.forEach((item) => {
				if (item.properties === null) {
					item.filteredProperties = {};

					return null;
				}

				const properties = Object.keys(item.properties);
				if (properties.includes("_original_variant")) {
					const original_variant =
						item.properties["_original_variant"];

					item.url = original_variant.url;
				}

				item.filteredProperties = properties.reduce(
					(result, property) => {
						if (property.startsWith("_")) {
							return result;
						}

						if (
							[
								"shipping_interval_unit_type",
								"shipping_interval_frequency"
							].includes(property)
						) {
							return result;
						}

						result[property] = item.properties[property];
						return result;
					},
					{}
				);

				if (
					properties.includes("shipping_interval_unit_type") &&
					properties.includes("shipping_interval_frequency")
				) {
					item.filteredProperties[
						"Subscription"
						] = `${item.properties["shipping_interval_frequency"]} ${item.properties["shipping_interval_unit_type"]}`;
				}
			});

			document.dispatchEvent(
				new CustomEvent("cart:item_count", {
					detail: {
						item_count: cart.item_count
					}
				})
			);

			state[STATE_CART] = cart;
		},
		[MUTATION_UPDATE_LOADING](state, loading) {
			state[STATE_LOADING] = loading;
		},
		[MUTATION_UPDATE_SETTINGS](state, settings) {
			state[STATE_SETTINGS] = settings;
		}
	},
	actions: {
		async [ACTION_INIT_CART](context) {
			context.commit(MUTATION_UPDATE_LOADING, true);
			context.commit(MUTATION_UPDATE_SETTINGS, settings.all());

			await context.dispatch(ACTION_GET_CART);

			context.commit(MUTATION_UPDATE_LOADING, false);
		},
		async [ACTION_GET_CART](context, { errorCallback = () => {} } = {}) {
			context.commit(MUTATION_UPDATE_LOADING, true);

			try {
				const cart = await api.getCart();

				context.commit(MUTATION_UPDATE_CART, cart);
				context.commit(MUTATION_UPDATE_LOADING, false);
			} catch (error) {
				await errorCallback(error);
			}
		},
		async [ACTION_ADD_TO_CART](
			context,
			{ products, errorCallback = () => {} } = {}
		) {
			context.commit(MUTATION_UPDATE_LOADING, true);

			try {
				await api.addItems(products);

				await context.dispatch(ACTION_GET_CART);

				context.commit(MUTATION_UPDATE_LOADING, false);

				document.dispatchEvent(
					new CustomEvent("track:cart:added", {
						detail: {
							// find items
						}
					})
				);
			} catch (error) {
				await errorCallback(error);
			}
		},
		async [ACTION_REMOVE_ITEM](
			context,
			{ key, errorCallback = () => {} } = {}
		) {
			context.commit(MUTATION_UPDATE_LOADING, true);

			try {
				await api.removeItem(key);

				await context.dispatch(ACTION_GET_CART);

				context.commit(MUTATION_UPDATE_LOADING, false);

				const item = context.state[STATE_CART].items.find((item) => {
					return item.key === key;
				});

				document.dispatchEvent(
					new CustomEvent("track:cart:removed", {
						detail: {
							item
						}
					})
				);
			} catch (error) {
				await errorCallback(error);
			}
		},
		async [ACTION_CHANGE_QUANTITY](
			context,
			{ key, quantity, errorCallback = () => {} } = {}
		) {
			context.commit(MUTATION_UPDATE_LOADING, false);

			try {
				await api.changeQuantity(key, quantity);

				await context.dispatch(ACTION_GET_CART);

				context.commit(MUTATION_UPDATE_LOADING, false);

				const item = context.state[STATE_CART].items.find((item) => {
					return item.key === key;
				});

				document.dispatchEvent(
					new CustomEvent("track:cart:changed-quantity", {
						detail: {
							item
						}
					})
				);
			} catch (error) {
				await errorCallback(error);
			}
		},
		async [ACTION_SAVE_FOR_LATER](
			context,
			{ key, errorCallback = () => {} } = {}
		) {
			context.commit(MUTATION_UPDATE_LOADING, false);

			const item = context.state[STATE_CART].items.find((item) => {
				return item.key === key;
			});

			if (!item) {
				return;
			}

			try {
				await Promise.all([
					context.dispatch(
						`wishlist/${ACTION_ADD_TO_WISHLIST}`,
						{
							product: item.product,
							variant: item.variant,
							errorCallback
						},
						{
							root: true
						}
					),
					context.dispatch(ACTION_REMOVE_ITEM, {
						key,
						errorCallback
					})
				]);

				await context.dispatch(ACTION_GET_CART);

				context.commit(MUTATION_UPDATE_LOADING, false);

				document.dispatchEvent(
					new CustomEvent("track:cart:saved-for-later", {
						detail: {
							item: item
						}
					})
				);
			} catch (error) {
				await errorCallback(error);
			}
		}
	}
};
