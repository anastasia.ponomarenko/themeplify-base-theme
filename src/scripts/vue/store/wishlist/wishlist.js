import {
	addToWishlist,
	getWishlistProducts,
	onWishlistLoad,
	removeFromWishList
} from "../../../core/helpers/swym";

export const STATE_LOADING = "loading";
export const STATE_WISHLIST = "wishlist";

export const MUTATION_UPDATE_LOADING = "updateLoading";
export const MUTATION_UPDATE_WISHLIST = "updateWishlist";

export const ACTION_INIT_WISHLIST = "initWishlist";
export const ACTION_GET_WISHLIST_PRODUCTS = "getWishlistProducts";
export const ACTION_ADD_TO_WISHLIST = "addToWishlist";
export const ACTION_REMOVE_FROM_WISHLIST = "removeFromWishlist";

export default {
	namespaced: true,
	state: () => {
		return {
			[STATE_LOADING]: true,
			[STATE_WISHLIST]: []
		};
	},
	getters: {},
	mutations: {
		[MUTATION_UPDATE_LOADING](state, loading) {
			state[STATE_LOADING] = loading;
		},
		[MUTATION_UPDATE_WISHLIST](state, products) {
			document.dispatchEvent(
				new CustomEvent("wishlist:item_count", {
					detail: {
						item_count: products.length
					}
				})
			);

			state[STATE_WISHLIST] = products;
		}
	},
	actions: {
		async [ACTION_INIT_WISHLIST](
			context,
			{ errorCallback = () => {} } = {}
		) {
			try {
				onWishlistLoad(async () => {
					await context.dispatch(ACTION_GET_WISHLIST_PRODUCTS, {
						errorCallback
					});
				});
			} catch (error) {}
		},
		async [ACTION_GET_WISHLIST_PRODUCTS](
			context,
			{ errorCallback = () => {} } = {}
		) {
			context.commit(MUTATION_UPDATE_LOADING, true);

			try {
				const products = await getWishlistProducts();

				context.commit(MUTATION_UPDATE_WISHLIST, products);
				context.commit(MUTATION_UPDATE_LOADING, false);
			} catch (error) {
				await errorCallback(error);
			}
		},
		async [ACTION_ADD_TO_WISHLIST](
			context,
			{ product, variant = null, errorCallback = () => {} } = {}
		) {
			context.commit(MUTATION_UPDATE_LOADING, true);

			try {
				await addToWishlist(product, variant);

				await context.dispatch(ACTION_GET_WISHLIST_PRODUCTS);
			} catch (error) {
				await errorCallback(error);
			}

			context.commit(MUTATION_UPDATE_LOADING, false);
		},
		async [ACTION_REMOVE_FROM_WISHLIST](
			context,
			{ product, variant = null, errorCallback = () => {} } = {}
		) {
			context.commit(MUTATION_UPDATE_LOADING, true);

			try {
				await removeFromWishList(product, variant);

				await context.dispatch(ACTION_GET_WISHLIST_PRODUCTS);
			} catch (error) {
				await errorCallback(error);
			}

			context.commit(MUTATION_UPDATE_LOADING, false);
		}
	}
};
